package kernel.words;

public class Word implements Comparable<Word> {
    protected String word;

    public Word(String word) {
        this.word = word.toLowerCase();
    }

    public String getWord() {
        return this.word;
    }

    @Override
    public int compareTo(Word o) {
        return word.compareTo(o.getWord());
    }

    public String toString() {
        return word;
    }
}
