package kernel.translate;

import java.util.List;

public class TranslateRequest {

    private Translation translation;
    private String selectedWord;
    private List<String> possibleTranslations;

    public TranslateRequest(Translation translation) {
        this.translation = translation;
        selectedWord = translation.getRandomWord();
        possibleTranslations = translation.getTranslationsOf(selectedWord);
    }

    public Translation getTranslation() {
        return translation;
    }

    public TranslateResponse setResponse(String response) {
        return new TranslateResponse(response);
    }

    public String getWord() {
        return selectedWord;
    }

    public List<String> getTranslations() {
        return possibleTranslations;
    }

    public String toString() {
        return "[Word: " + selectedWord + ", Translations: " + possibleTranslations + "]";
    }

    public class TranslateResponse {
        private String response;

        public TranslateResponse(String response) {
            this.response = response;
        }

        public boolean isValidTranslation() {
            return getTranslations().contains(response);
        }

        public Translation getTranslation() {
            return translation;
        }
    }
}
