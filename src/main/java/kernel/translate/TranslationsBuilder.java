package kernel.translate;

import kernel.user.User;

import java.io.*;


public class TranslationsBuilder {
    private static String resourcePath;


    public static void main(String[] args) {
        initialize("/TranslationsRecord.txt");
        User user = new User("");
        System.out.println(user.getTranslationsManager());

    }

    public static void initialize(String resourcePath) {
        TranslationsBuilder.resourcePath = resourcePath;

    }

    public static TranslationsManager buildTranslationsManager(User user) {

        TranslationsManager manager = new TranslationsManager(user);

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        TranslationsBuilder.class.getResourceAsStream(resourcePath)));

        reader.lines().forEach(line -> {
            String[] words = line.split(",");
            for (int i = 0; i < words.length; ++i) {
                String word = words[i];
                word = word.toLowerCase().trim();
                words[i] = word;
            }
            Translation translation = new Translation(words);
            manager.addTranslation(translation);
        });
        return manager;
    }



    public static Translation createTranslation(String... words) {
        return new Translation(words);
    }
}
